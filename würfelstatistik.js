var rollButton = document.querySelector(".rollButton")
var resultsTable = document.querySelector(".results")
var advancedEyes = document.querySelector(".advancedEyes")
var advancedNum = document.querySelector(".advancedNum")
var advancedSame = document.querySelector(".advancedSame")
var map = new Map()

function rollButtonClick(){
    map = new Map()
    var eyes
    var times
    var same

    eyes = checkOrDeafult(advancedEyes.value,6)
    times = checkOrDeafult(advancedNum.value,100)
    same = checkOrDeafult(advancedSame.value,2)

    generateAndRoll(eyes, times, same)

}

function generateAndRoll(eyes, times, same){
    for (let i=0;i<times;i++){
        var tempRoll = rollCustom(eyes,same)
        if(map.get(tempRoll)){
            map.set(tempRoll,map.get(tempRoll) + 1)
        }
        else{
            map.set(tempRoll,1)
        }
    }
    generateTable(eyes)
}

function generateTable(eyes){
    var tableString = "<tr> <th>Augen</th> <th>Anzahl</th>  </tr>"
    var keys = []
    for (let key of map){
        keys.push(key[0])
    }
    var big = keys[0]
    var smal = keys[0]
    for(i=0;i<keys.length;i++){
        big = Math.max(big,keys[i])
        smal = Math.min(smal,keys[i])
    }
    for(i=smal;i<=big;i++){
        var res
        if(map.get(i)){
            res = map.get(i)
        }
        else{
            res = 0
        }
        tableString = ( tableString + 
            "<tr>" + "<td>" + i + "</td>" + "<td class='" + "result" + i + "'>"+  res + "</td>"+ "</tr>")
    }
    resultsTable.innerHTML = tableString
}

function checkOrDeafult(variable, defalut){
    if (!variable){
        return defalut
    }
    else{
        return variable
    }
}

function rollCustom(eyes, same){
    var result = 0

    for (let i=0;i<same;i++){
        result += roll(eyes)
    }

    return result
}

function roll(sides){
    return Math.floor(Math.random() * sides) + 1;
}

rollButton.addEventListener('click',rollButtonClick)